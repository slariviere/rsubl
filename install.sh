 # Author: Sébastien Larivière (sebastien@lariviere.me)
 # Date: 2012-12-20
 # Filename: install.sh
 # Summary: Intall rsubl on remote servers

#!/usr/bin/env bash

FILE_NAME="rsubl"
BASE_INSTALL_DIR="/usr/local/bin/"

FILE_URL="https://raw.github.com/aurora/rmate/master/rmate"
FILE_URL_NAME="rmate"

if [[ `command -v wget` ]]; then
    wget $FILE_URL
    mv $FILE_URL_NAME $FILE_NAME
elif [[ `command -v curl` ]]; then
    curl $FILE_URL > $FILE_NAME
fi

mv $FILE_NAME $BASE_INSTALL_DIR && chmod u+x "$BASE_INSTALL_DIR$FILE_NAME" && echo "Intallation Completed."